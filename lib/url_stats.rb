class UrlStats
  def initialize(url_pattern, file_lines)
    @url_pattern = url_pattern
    url_regex = Regexp.new(url_pattern)
    @lines = file_lines.select { |line| line =~ /#{url_pattern}/ }
  end

  def show
    url_text = "URL - #{@url_pattern.gsub('\d+', '{user_id}')}\n" +
    "-----------------------------------------\n No Requests Found"
    return url_text if requests.empty?
    "URL - #{@url_pattern.gsub('\d+', '{user_id}')}\n" +
    "-----------------------------------------\n" +
    "No of requests - #{no_of_requests}\n" +
    "Mean response time - #{mean_response_time}ms\n" +
    "Median response time - #{median_response_time}ms\n" +
    "Mode of response time - #{mode_of_reponse_time}ms\n" +
    "Most responding dynamo - #{most_repsonding_dynamo}\n"
  end

  private

  def requests
    @requests ||= @lines.map { |line| Request.new(line) }
  end

  def no_of_requests
    @no_of_requests ||= requests.length
  end

  def mean_response_time
    requests.map(&:response_time).inject(:+)/no_of_requests
  end

  def median_response_time
    sorted_times = requests.map(&:response_time).sort
    (sorted_times[(no_of_requests - 1) / 2] + sorted_times[no_of_requests / 2]) / 2
  end

  def mode_of_reponse_time
    mode requests.map(&:response_time)
  end

  def most_repsonding_dynamo
    mode requests.map(&:dyno)
  end

  def mode(arr)
    frequency = arr.each_with_object({}) do |value, hsh|
      hsh[value] ||= 0
      hsh[value] += 1
    end
    frequency.max_by{|_,v| v }.first
  end
end
