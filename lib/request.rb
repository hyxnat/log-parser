class Request
  def initialize(line)
    @line_array = line.split
  end

  def dyno
    @line_array.find{ |word| word =~ /dyno=/ }.split('=').last
  end

  def response_time
    connect_time + service_time
  end

  private

  def connect_time
    @line_array.find{ |word| word =~ /connect=/ }[/\d+/].to_i
  end

  def service_time
    @line_array.find{ |word| word =~ /service=/ }[/\d+/].to_i
  end
end
