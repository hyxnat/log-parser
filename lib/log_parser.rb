require 'yaml'

require_relative 'url_stats'
require_relative 'request'

class LogParser
  def initialize(file_path)
    @file_path = file_path
    @url_list = YAML.load_file(File.expand_path('../url_list.yml', __FILE__))
  end

  def execute
    @url_list.each do |url|
      puts "-----------------------------------------\n"
      puts UrlStats.new(url, all_requests).show
      puts "\n"
    end
  end

  private

  def all_requests
    @all_requests ||= File.readlines(@file_path)
  end
end
