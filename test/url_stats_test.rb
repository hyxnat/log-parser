require_relative 'test_helper'

describe UrlStats do
  describe 'show' do
    it 'should show stats' do
      file_lines = File.readlines(File.expand_path('../log.txt', __FILE__))
      stat = UrlStats.new('method=GET path=/api/users/\d+/count_pending_messages ', file_lines)

      result = "URL - method=GET path=/api/users/{user_id}/count_pending_messages \n" +
               "-----------------------------------------\n" +
               "No of requests - 2\n" +
               "Mean response time - 33ms\n" +
               "Median response time - 33ms\n" +
               "Mode of response time - 33ms\n" +
               "Most responding dynamo - web.12\n"

      stat.show.must_equal result
    end

    it 'should show stats' do
      file_lines = File.readlines(File.expand_path('../log.txt', __FILE__))
      stat = UrlStats.new('method=POST path=/api/users/\d+ ', file_lines)

      result = "URL - method=POST path=/api/users/{user_id} \n" +
               "-----------------------------------------\n No Requests Found"

      stat.show.must_equal result
    end
  end
end
