require 'bundler/setup'
require 'minitest/spec'
require 'minitest/autorun'
require 'minitest/reporters'

require_relative '../lib/log_parser'
require_relative '../lib/request'
require_relative '../lib/url_stats'
