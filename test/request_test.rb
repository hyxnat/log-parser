require_relative 'test_helper'

describe Request do
  before do
    @line = "2014-01-09T06:16:53.748849+00:00 heroku[router]: at=info method=POST " +
            "path=/api/online/platforms/facebook_canvas/users/100002266342173/add_ticket " +
            "host=services.pocketplaylab.com fwd='94.66.255.106' dyno=web.12 connect=12ms " +
            "service=21ms status=200 bytes=78"
  end

  describe 'dyno' do
    it 'must return the dyno' do
      Request.new(@line).dyno.must_equal 'web.12'
    end
  end

  describe 'response_time' do
    it 'must return the response_time' do
      Request.new(@line).response_time.must_equal 33
    end
  end
end
